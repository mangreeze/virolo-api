/**
 * Express Module providing local authentication
 * @module routes/users
 * @require express
 * @require mongoose
 */

/**
 * express module
 * @const
 */
const express = require('express');

/**
 * Express router to mount routes on
 * @type {object}
 * @const
 * @namespace router
 */
const router = express.Router();

/**
 * passport module that manage session
 * @const
 */
const passport = require('passport');

/**
 * ensure module that check permissions on certain routes
 * @const
 */
const ensure = require('../public/js/protect');

/**
 * ensure module that contains all globals
 * @const
 */
const globals = require('../public/js/globals');
global.UserEmail = undefined;

/**
 * passport local strategy to authenticate
 * @const
 */
const LocalStrategy = require('passport-local').Strategy;
const GoogleStrategy = require('passport-google-oauth2').Strategy;
/*const FacebookStrategy = require('passport-facebook').Strategy; */

/**
 * Load User model from mongoose
 * @const
 */
const User = require('../models/user');

const Test = require('../models/passwordChange');

setImmediate(CreateAdmin);

/**
 * Call the framework mutler, and instantiate is options for image uploading
 * @const
 */
const multer = require('multer');
const fileFilter = (req, file, cb) => {
  if (file.mimetype === 'image/jpeg' || file.mimetype === 'image/png')
    cb(null, true);
  else
    cb(null, false);
}
const storage = multer.diskStorage({
  destination : function(req, file, cb) {
    cb(null, 'public/img');
  },
  filename: function(req, file, cb) {
    cb(null, file.originalname.replace(' ', '_').toLowerCase());
  }
});
const upload = multer({storage: storage, fileFilter: fileFilter});

/**
 * Create an admin account on database creation
 * @function
 */
function CreateAdmin() {
  Test.SendMailToUser();
  User.CreateFirstAdminAccount();
}

/**
 * Retrieve the list of users<p></p>
 * Use an User schema method to parse the list of users from mongo db
 * @name / - GET
 * @function
 * @memberof module:routes/users
 * @inner
 * @param {string} path - Express path
 * @param {function} permission - ensure permissions to access the route
 * @param {callback} callback - Express middleware
 */
router.get('/', ensure.adminRights, function(req, res) {
  User.getUsers(res);
});

/**
 * Retrieve one user<p></p>
 * Use an User schema method to get an user from mongo db
 * @name / - GET
 * @function
 * @memberof module:routes/users
 * @inner
 * @param {string} path - Express path
 * @param {callback} callback - Express middleware
 */
router.get('/:id', function(req, res) {
  User.getUser(req.params.id, res)
});

/**
 * Give admin permissions to a specified user<p></p>
 * Use an User schema method to find the User and modify it in mongo db
 * @name /:name - PUT
 * @function
 * @memberof module:routes/users
 * @inner
 * @param {string} path - Express path
 * @param {function} permission - ensure permissions to access the route
 * @param {callback} callback - Express middleware
 */
router.put('/:name/admin', ensure.adminRights, function(req, res) {
  User.giveAdminRights(req.params.name);
});

router.put('/:id/image', ensure.authenticated, upload.single('image'), function(req, res) {
  if (req.file == undefined)
  return res.status(400).json({success: 'ko', msg: 'Did not recieved an uploaded file'});
  User.addImage(req, res);
});

/**
 * Delete the specified User<p></p>
 * Use an User schema method find the User and delete him.
 * Another method is then Used to erase his Packs.
 * @name /:name - DELETE
 * @function
 * @memberof module:routes/users
 * @inner
 * @param {string} path - Express path
 * @param {function} permission - ensure permissions to access the route
 * @param {callback} callback - Express middleware
 */
router.delete('/:name', ensure.adminRights, function(req, res) {
  User.eraseAccount(req.params.name);
});

/**
 * Register a new User in mongo db<p></p>
 * Registering a new user is made possible only for the teachers, 
 * this is why you must be logged before accessing this route.
 * @name /register - POST
 * @function
 * @memberof module:routes/users
 * @inner
 * @param {string} path - Express path
 * @param {function} permission - ensure permissions to access the route
 * @param {callback} callback - Express middleware
 */
router.post('/register', function(req, res) {
  req.checkBody('name', 'Name is required').notEmpty();
  req.checkBody('email', 'Email is required').notEmpty();
  req.checkBody('email', 'Email is not valid').isEmail();
  req.checkBody('password', 'Password is required').notEmpty();
  req.checkBody('password2', 'Passwords do not match').equals(req.body.password);
  var errors = req.validationErrors();
	if(errors){
		res.status(500).send({success:'ko', msg:'Could not register the new user'});
	} else {
		var newUser = new User({
			name: req.body.name,
			email: req.body.email,
      password: req.body.password,
      //avatar: "",
      permission : false
		});
    User.createUser(newUser, req, res);
  }
});

/**
 * Make passport use this callback when logging in when specifying 'google' strategy
 * @function
 */
/* passport.use(new GoogleStrategy({
  clientID: globals.googleAuth.clientID,
  clientSecret: globals.googleAuth.clientSecret,
  callbackURL: globals.googleAuth.redirectURL,
  passReqToCallback: true
  },
  function(req, accessToken, refreshToken, profile, done) {
    process.nextTick(function() {
      if (req.user) {
      User.findOne({'name': req.user.name}, function(err, user) {
        if (err)
          return done(err);
        if (user) {
          req.user.google.id = profile.id;
          req.user.google.token = accessToken;
          req.user.save(function(err) {
            if (err)
              throw err;
            return done(null, req.user);
          })
        } else { return done(null, user); }
      })
      } else {
        var newUser = new User({
          name: profile.displayName,
          email: profile.emails[0].value,
          avatar: "",
          google: { token: accessToken, id: profile.id}
        });
        User.createUser(newUser, req, res);
      }
    })
  })); 
  */
/**
 * Make passport use this callback when logging in when specifying 'facebook' strategy
 * @function

passport.use(new FacebookStrategy({
  clientID: globals.facebookAuth.clientID,
  clientSecret: globals.facebookAuth.clientSecret,
  },
  function(accessToken, refreshToken, profile, done) {
    if (req.user) {
      User.findOne({'name': req.user.name}, function(err, user) {
        if (err)
          return done(err);
        if (user) {
          req.user.facebook.id = profile.id;
          req.user.facebook.token = accessToken;
          req.user.save(function(err) {
            if (err)
              throw err;
            return done(null, req.user);
          })
        } else {
          var newUser = new User({
            name: profile.name.givenName,
            email: profile.emails[0].value,
            facebook: {token: accessToken, id: profile.id}
          });
          User.createUser(newUser, req, res);
        }
      });
    }
  }));
  */

/**
 * Make passport use this callback when logging in when specifying 'local' strategy
 * @function
 */
passport.use (new LocalStrategy({
  usernameField: 'email',
  passwordField: 'password'
},
  function(email, password, done) {
    User.getUserByEmail(email, function(err, user) {
      if (err) throw err;
      if (!user) {
        return done(null, false, {message: 'Unknown Email'});
      }
      User.comparePassword(password, user.password, function(err, isMatch){
        if (err) throw err;
        if (isMatch) {
          UserEmail = email
          return done(null, user);
        } else {
          return done(null, false, {message: 'Invalid password'});
        }
      });
    });
  }));

/**
 * determines wich data of the user must be store into a session
 * @function
 */
passport.serializeUser(function(user, done){
  done(null, user.id);
});

/**
 * used to retrieve the whole object user when needed
 * @function
 */
passport.deserializeUser(function(id, done) {
  User.findById(id, function(err, user) {
    done(err, user);
  });
});

/**
 * Route to login a existing user and store a session<p></p>
 * The expected format of the request body is JSON :<p></p>
 * email - contain the email of the user<p></p>
 * password - contain the password of the user<p></p>
 * @name /login - POST
 * @function
 * @memberof module:routes/users
 * @inner
 * @param {string} path - Express path
 * @param {callback} middleware - Express middleware
 */
router.post('/login', function(req, res, next) {
  passport.authenticate('local', function (err, user) {
    req.logIn(user, function() {
        res.status(!user ? 500 : 200).send(!user ? {success:'ko', msg:"Invalid Email or Password"} : {success:'ok', msg:'You are now logged in'});
    });
  })(req, res, next);
});

/**
 * Route to login with google using passport google strategy
 * @name /login/google
 * @function
 * @memberof module:routes/getUsers
 * @inner
 * @param {string} path - Express path
 * @param {callback} middleware - Passport middleware
 */
router.post('/login/google', passport.authenticate('google', { scope: ['profile', 'email']}));


/**
 * Callback used by goole api when connected using passport google strategy
 * @name /login/google/callback
 * @function
 * @memberof module:routes/getUsers
 * @inner
 * @param {string} path - Express path
 * @param {callback} middleware - Express middleware
 */
router.post('/login/google/callback', function(req, res, next) {
  passport.authenticate('google', function (err, user) {
    req.logIn(user, function() {
        res.status(!user ? 500 : 200).send(!user ? {success:'ko', msg:"Invalid Email or Password"} : {success:'ok', msg:'You are now logged in'});
    });
  })(req, res, next);
});

/**
 * Route to logout from the session
 * @name /users/logout - GET
 * @function
 * @memberof module:routes/users
 * @inner
 * @param {string} path - Express path
 * @param {callback} middleware - Express middleware 
 */
router.get('/logout', function(req, res){
  req.logout();
  res.status(200).send({success: 'ok', msg: 'You have been logged out.'});
});

module.exports = router;