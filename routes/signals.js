const express = require('express');
const router = express.Router();
const ensure = require('../public/js/protect');
const Signals = require('../models/signals');

router.get('/', function(req, res) {
  Signals.getSignals(res);
});

router.post('/', function(req, res) {
  body = req.body;
  if (body.latitude == undefined || body.longitude == undefined) {
    return res.json(500).json({success: 'ko', msg: 'invalidField'});
  } else {
    var newSignal = new Signals({
      latitude: body.latitude,
      longitude: body.longitude,
      date: new Date()
    });
    Signals.createSignal(newSignal, res);
  }
});

module.exports = router;