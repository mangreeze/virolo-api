/**
 * Express Module providing authentication routes to external services
 * @module main
 * @require express
 * @require mongoose
 * @require nodejs
 */

const express = require('express');
const cors = require('cors');
const path = require('path');
const cookieParser = require('cookie-parser');
const cookieSession = require('cookie-session');
const bodyParser = require('body-parser');
const expressValidator = require('express-validator');
const flash = require('connect-flash');
const session = require('express-session');
const passport = require('passport');
const mongo = require('mongodb');
const mongoose = require('mongoose');

mongoose.Promise = global.Promise;

mongoose.connect('mongodb://mongo:27017', {
  useMongoClient: true
});

var db = mongoose.connection;

const users = require('./routes/users');
const signals = require('./routes/signals');

/**
 * make the server use express
 * @const
 */
const app = express();

app.use(cors({credentials: true, origin: true}));
app.use(express.static(path.join(__dirname, 'public')));
app.use(cookieParser());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(session({secret: 'virolo_super_secret', saveUninitialized: true, resave: true}));
app.use(passport.initialize());
app.use(passport.session());

app.use(expressValidator({
  errorFormatter: function(param, msg, value) {
      var namespace = param.split('.')
      , root    = namespace.shift()
      , formParam = root;

    while(namespace.length) {
      formParam += '[' + namespace.shift() + ']';
    }
    return {
      param : formParam,
      msg   : msg,
      value : value
    };
  }
}));

app.use(flash());
app.use(function (req, res, next) {
  res.locals.success_msg = req.flash('success_msg');
  res.locals.error_msg = req.flash('error_msg');
  res.locals.error = req.flash('error');
  res.locals.user = req.user || null;
  next();
});

app.use('/api/users', users);
app.use('/api/signals', signals);

app.set('port', (process.env.PORT || 8080));
app.listen(app.get('port'),function(){
	console.log('Server started on port '+app.get('port'));
});