function authenticated(req, res, next){
	if(req.isAuthenticated()){
		return next();
	} else {
		return res.status(401).send({success: 'ko', msg: 'You must be authenticated to access this route.'});
	}
}

function adminRights(req, res, next) {
	if (req.isAuthenticated() && req.user.permission == true) {
		return next();
	} else {
		return res.status(401).send({success: 'ko', msg: 'You do not have the permission to access this route'});
	}
}

module.exports.authenticated = authenticated;
module.exports.adminRights = adminRights;