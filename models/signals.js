var mongoose = require('mongoose');

mongoose.connect('mongodb://localhost:27017/User', {
  useMongoClient : true,
});

var rawSignalsSchema = new mongoose.Schema({
  longitude: Number,
  latitude: Number,
  date: Date,
});

/*var dangerAreaSchema = new mongoose.Schema({
  longitude: Number,
  latitude: Number,
  signals: {
    date: Date
  },
  level: String
});*/

var rawSignals = module.exports = mongoose.model('signals', rawSignalsSchema);

module.exports.getSignals = function(res) {
  var tmp = rawSignals.find({});
  tmp.exec(function(err, items) {
    if (err) {
      return res.status(500).json({success: 'ko', msg: 'Cannot get the signals list'});
    }
    var table = [];
    for (var i = 0; i != items.length; i++) {
      table.push({id: items[i]._id, latitude: items[i].latitude, longitude: items[i].longitude, date: items[i].date});
    };
    return res.status(200).json({success: 'ok', data: table});
  });
}

module.exports.createSignal = function(signal, res) {
  rawSignals.create(signal, function (err) {
    if (err) {
      return res.status(500).json({success: 'ko', msg: 'Cannot add a new signal'});
    } else {
      return res.status(200).json({success: 'ok', msg: 'Added a signal'});
    }
  });
}

module.exports.deleteSignal = function(id, res) {
  rawSignals.remove({_id: id}, function(err) {
    if (err) {
      return res.status(500).json({success: 'ko', msg: 'Could not delete the specified item'});
    } else {
      return res.status(200).json({success: 'ok', msg: 'Item deleted'});
    }
  });
}