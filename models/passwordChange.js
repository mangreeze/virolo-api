/*
//  Fichier contenant l'ensemble des méthodes permettant de gérer 
//  le remplacement de mot de passe en cas de perte de celui ci 
//  ou demande de changement par l'utilisateur
*/

var nodemailer = require('nodemailer');

var mongoose = require('mongoose');

mongoose.connect('mongodb://localhost:27017', {
    useMongoClient : true,
});


/*
//  Schéma permettant de stocker l'ensemble des personnes ayant demandé
//  un changement de mot de passe 
*/

var PasswordChangeSchema = new mongoose.Schema({
    // id de l'utilisateur correspondant à celui spécifié dans 
    // la table de donnée User
    idUser: String,
    // email de l'utilisateur 
    email: String, 
    // Date de fin de validité pour la demande de changement de mot de passe
    // Si la date a été dépassé alors, cet enregistrement est effacé
    dateEnd: Date 
});

var PasswordChange = module.exports = mongoose.model('passwordChange',PasswordChangeSchema);

module.exports.SendMailToUser = function() {

    // create reusable transporter object using the default SMTP transport
    let transporter = nodemailer.createTransport({
        service: 'gmail',
        auth: {
            user: 'clement.beca@gmail.com', // generated ethereal user
            pass: 'NCq5Vb4x' // generated ethereal password
        }
    });

    // send mail with defined transport object
    transporter.sendMail({
        from: 'clement.beca@gmail.com',
        to: 'clement.becamel@epitech.eu', // list of receivers
        subject: 'EIP', // Subject line
        text: 'Tu as bossé sur l\'EIP ?', // plain text body
        html: '<b>Tu as bossé sur l\'EIP ?</b>' // html body
    });
}

/**
 *  Méthode permettant de Créer une nouvelle entrée dans la base de donnée
 *  @function
 *  @param {PasswordChangeSchema} newPasswordChange - Information sur la nouvelle
 *  entrée à ajouter dans la base de donnée, l'heure n'est pas renseigné
 */
module.exports.createUser = function(newPasswordChange, req, res) {
    var tmp = PasswordChange.find({idUser: newPasswordChange.idUser});
    tmp.exec(function(err, count) {
        if (count.length == 0) {
            // Ajoute l'heure de fin de validité de l'entrée
            //newPasswordChange.dateEnd = ;
            //newPasswordChange.save();
            res.status(200).send({success: 'ok', msg: 'Un message a été envoyé sur votre boite mail.'});
        } else {
            // Ajoute l'heure de fin de validité de l'entrée
            //tmp.dateEnd = ;
            res.status(200).send({success: 'ok', msg: 'Un message a été envoyé sur votre boite mail.'});
        }
    })
}