var mongoose = require('mongoose');
var bcrypt = require('bcryptjs');

mongoose.connect('mongodb://localhost:27017/User', {
  useMongoClient : true,
});

var UserSchema = new mongoose.Schema({
  email: {type: String, index: true},
  password: String,
  avatar: String,
  name: String,
  google: {
    id: String,
    token: String
  },
  facebook: {
    id: String,
    token: String
  },
  permission: Boolean
});

var User = module.exports = mongoose.model('users', UserSchema);

module.exports.createUser = function(newUser, req, res) {
  var tmp = User.find({email : newUser.email});
  tmp.exec(function(err, count) {
    if (count.length == 0) {
      bcrypt.genSalt(10, function(err, salt) {
        bcrypt.hash(newUser.password, salt, function(err, hash) {
          newUser.password = hash;
          newUser.save();
          res.status(200).send({success: 'ok', msg: 'You are now registered and can login'});
        });
      });
    } else {
      res.status(500).send({success: 'ko', msg: 'We failed to create the ressource'});
    }
  });
};

module.exports.getUserByEmail = function(email, callback) {
  var query = {email: email};
  User.findOne(query, callback);
};

module.exports.getUserById = function(id, callback) {
  User.findById(id, callback);
};

module.exports.comparePassword = function(candidatePassword, hash, callback) {
  bcrypt.compare(candidatePassword, hash, function(err, isMatch) {
    if (err) {throw err;}
    callback(null, isMatch);
  });
};

module.exports.getUsers = function(res) {
  var tmp = User.find({});
  tmp.exec(function(err, count) {
    var table = [];
    for (var i = 0; i != count.length; i++) {
      if (count[i].name != 'admin')
        table.push({name: count[i].name, id: count[i]._id});
    }
    res.status(201).json(table);
  });
}

module.exports.getUser = function(id, res) {
  var tmp = User.findOne({_id: id});
  tmp.exec(function(err, user) {
    if (err)
      { throw err; }
    if (user) {
      delete user['password'];
      res.status(201).json({success: 'ok', user: user});
    } else {
      res.status(400).json({success: 'ko', msg: 'Could not found the user'});
    }
  });
}

module.exports.giveAdminRights = function(name) {
  User.update({name: name}, {permission: true}).exec();
}

module.exports.addImage = function(req, res) {
  var tmp = User.findOne({_id: req.params.id});
  tmp.exec(function(err, user) {
    if (user) {
      var imagePath = 'img/' + req.file.originalname.replace(' ', '_').toLowerCase();
      user.avatar = imagePath;
      user.save(function(err) {
        if (err)
          return res.status(500).json({sucess: 'ko', msg: 'Could not save the file in db'});
        else
          return res.status(200).json({success: 'ok', msg: 'Image successfully uploaded'});
      });
    } else {
      res.status(400).json({success: 'ko', msg: 'Could not found the user'});
     }
  })
}

module.exports.getEmail = function(name) {
  var tmp = User.find({name:name});
  return tmp;
}

module.exports.eraseAccount = function(name, callback) {
  User.remove({name: name}).exec();
}

module.exports.CreateFirstAdminAccount = function() {
  var tmp = User.find({});
  tmp.exec(function(err, count) {
    if (count.length == 0) {
      var admin = new User();
      admin.email = 'admin@admin.com';
      admin.name = 'The Administrator';
      admin.avatar = "";
      admin.permission = true;
      bcrypt.genSalt(10, function(err, salt) {
        bcrypt.hash('virolo2019api', salt, function(err, hash) {
          admin.password = hash;
          admin.save();
        });
      });
    }
  });
}